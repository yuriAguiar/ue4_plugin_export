// Fill out your copyright notice in the Description page of Project Settings.


#include "PluginExportComponent.h"
#include "Engine/Engine.h"

// Sets default values for this component's properties
UPluginExportComponent::UPluginExportComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}


// Called when the game starts
void UPluginExportComponent::BeginPlay()
{
	Super::BeginPlay();
	GLog->Log(GetOwner()->GetName() + ": " + Text);
	UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *GetOwner()->GetName(), *Text);
	GEngine->AddOnScreenDebugMessage(-1, DisplayTime, FColor::Cyan, GetText());
}


// Called every frame
void UPluginExportComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

FString UPluginExportComponent::GetText() const
{
	return GetOwner()->GetName() + ": " + Text;
}
