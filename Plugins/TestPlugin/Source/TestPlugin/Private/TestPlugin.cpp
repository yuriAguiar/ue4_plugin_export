// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TestPlugin.h"
#include "PropertyEditor/Public/PropertyEditorModule.h"
#include "TestPlugin/Public/Bool9PatchDetails.h"
#include "PluginExportComponent.h"
#include "PaperGroupedSpriteComponent.h"
#include "PaperGroupedSpriteInstacesHider.h"
#include "../Plugins/2D/Paper2D/Source/Paper2D/Classes/PaperGroupedSpriteComponent.h"
 	
#define LOCTEXT_NAMESPACE "FTestPluginModule"

void FTestPluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	UE_LOG(LogTemp, Warning, TEXT("### Bool9Patch Details Loaded ###"));
	FPropertyEditorModule& PropertyEditorModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyEditorModule.RegisterCustomClassLayout(UPluginExportComponent::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&Bool9PatchDetails::MakeInstance));
	PropertyEditorModule.RegisterCustomClassLayout(UPaperGroupedSpriteComponent::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&PaperGroupedSpriteInstacesHider::MakeInstance));
}

void FTestPluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	FPropertyEditorModule& PropertyEditorModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyEditorModule.UnregisterCustomClassLayout(UPluginExportComponent::StaticClass()->GetFName());
	PropertyEditorModule.UnregisterCustomClassLayout(UPaperGroupedSpriteComponent::StaticClass()->GetFName());
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FTestPluginModule, TestPlugin)