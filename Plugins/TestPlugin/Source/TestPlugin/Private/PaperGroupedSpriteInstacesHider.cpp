// Fill out your copyright notice in the Description page of Project Settings.


#include "PaperGroupedSpriteInstacesHider.h"
#include "PropertyEditor/Public/DetailCategoryBuilder.h"
#include "PropertyEditor/Public/DetailLayoutBuilder.h"

#define LOCTEXT_NAMESPACE "PaperGroupedSpriteInstacesHider"

PaperGroupedSpriteInstacesHider::PaperGroupedSpriteInstacesHider() { }

PaperGroupedSpriteInstacesHider::~PaperGroupedSpriteInstacesHider() { }

TSharedRef<IDetailCustomization> PaperGroupedSpriteInstacesHider::MakeInstance()
{
    return MakeShareable(new PaperGroupedSpriteInstacesHider);
}

void PaperGroupedSpriteInstacesHider::CustomizeDetails(IDetailLayoutBuilder& DetailLayoutBuilder)
{
    IDetailCategoryBuilder& InstancesCategory = DetailLayoutBuilder.EditCategory("Instances", LOCTEXT("Instances", "Instances"), ECategoryPriority::Important);
    // InstancesCategory.SetCategoryVisibility(false);
}

#undef LOCTEXT_NAMESPACE
