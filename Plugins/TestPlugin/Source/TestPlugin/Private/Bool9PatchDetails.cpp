// Fill out your copyright notice in the Description page of Project Settings.

#include "TestPlugin/Public/Bool9PatchDetails.h"
#include "PluginExportComponent.h"
#include "PropertyCustomizationHelpers.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include "PropertyEditor/Public/DetailCategoryBuilder.h"
#include "PropertyEditor/Public/DetailLayoutBuilder.h"
#include "PropertyEditor/Public/DetailWidgetRow.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Input/SButton.h"
#include "Input/Reply.h"
#include "Media/Public/IMediaTracks.h"
#include "Widgets/Input/SCheckBox.h"
#include "Widgets/Input/SEditableTextBox.h"
#include "Widgets/Input/SSpinBox.h"

#define LOCTEXT_NAMESPACE "Bool9PatchDetails"

Bool9PatchDetails::Bool9PatchDetails() { }

Bool9PatchDetails::~Bool9PatchDetails() { }

TSharedRef<IDetailCustomization> Bool9PatchDetails::MakeInstance()
{
    return MakeShareable(new Bool9PatchDetails);
}

void Bool9PatchDetails::CustomizeDetails(IDetailLayoutBuilder& DetailLayoutBuilder)
{
    DetailLayoutBuilder.GetObjectsBeingCustomized(Objects);
    
    PluginExportComponentObject = Cast<UPluginExportComponent>(Objects[0].Get()); // Global member to use in functions
    TWeakObjectPtr<UPluginExportComponent> PluginExportComponentObjectLocal = Cast<UPluginExportComponent>(Objects[0].Get()); // Local member to use in lambdas
    // Objects = DetailLayoutBuilder.GetSelectedObjects();

    IntPairPropertyHandle = DetailLayoutBuilder.GetProperty(GET_MEMBER_NAME_CHECKED(UPluginExportComponent, IntPairStruct));
    DisplayTimePropertyHandle = DetailLayoutBuilder.GetProperty(GET_MEMBER_NAME_CHECKED(UPluginExportComponent, DisplayTime));
    IDetailCategoryBuilder& CategoryBuilder = DetailLayoutBuilder.EditCategory("Settings|Bool9Patch", LOCTEXT("CategoryName", "Bool9Patch Settings"), ECategoryPriority::Important);

    // Getting struct properties using child handles
    auto IntPairVal1Property = IntPairPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FIntPairStruct, Val1));
    auto IntPairVal2Property = IntPairPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FIntPairStruct, Val2));

    // ### START Simple float property using lambdas ###
    CategoryBuilder.AddCustomRow(LOCTEXT("DisplayTime", "Display Time"))
    .WholeRowContent()
    [
        SNew(STextBlock)
        .Text(LOCTEXT("DisplayTimeText", "Display Time Text"))
    ];

    auto PrintDisplayTimeLambda = [PluginExportComponentObjectLocal]
    {
        // UE_LOG(LogTemp, Warning, TEXT("Bool9PatchDetails::PrintLambda %s"), *PluginExportComponentObject->GetName());
        return FText::FromString(FString::SanitizeFloat(PluginExportComponentObjectLocal->DisplayTime));
    };

    auto SetDisplayTimeLambda = [PluginExportComponentObjectLocal] (FText DisplayTimeText)
    {
        PluginExportComponentObjectLocal->DisplayTime = FCString::Atof(*DisplayTimeText.ToString());
        UE_LOG(LogTemp, Warning, TEXT("DisplayTime current value: %f"), PluginExportComponentObjectLocal->DisplayTime);
    };

    CategoryBuilder.AddCustomRow(LOCTEXT("DisplayTimeValue", "Display Time Value"))
    .WholeRowContent()
    [
        SNew(SEditableTextBox)
        .Text_Lambda(PrintDisplayTimeLambda)
        .OnTextChanged_Lambda(SetDisplayTimeLambda)
    ];
    // ### END Simple float property using lambdas ###

    // ### START Complex struct property using raw functions ###
    CategoryBuilder.AddCustomRow(LOCTEXT("IntPair", "Int Pair"))
    .NameContent()
    [
        SNew(STextBlock)
        .Text(LOCTEXT("IntPairText", "Int Pair"))
        .Font(IDetailLayoutBuilder::GetDetailFont())
    ]
    .ValueContent().MaxDesiredWidth(50.0f)
    [
        SNew(SHorizontalBox) +
        SHorizontalBox::Slot()
        [
            SNew(SProperty, IntPairVal1Property).CustomWidget()
            [
                SNew(SHorizontalBox) +
                SHorizontalBox::Slot()
                [
                    SNew(SEditableTextBox)
                    .Text_Raw(this, &Bool9PatchDetails::GetVal1FromProperty)
                    .OnTextCommitted_Raw(this, &Bool9PatchDetails::SetVal1OnProperty)
                ]
            ]
        ]
        +
        SHorizontalBox::Slot()
        [
            SNew(SProperty, IntPairVal2Property).CustomWidget()
            [
                SNew(SHorizontalBox) +
                SHorizontalBox::Slot()
                [
                    SNew(SEditableTextBox)
                    .Text_Raw(this, &Bool9PatchDetails::GetVal2FromProperty)
                    .OnTextCommitted_Raw(this, &Bool9PatchDetails::SetVal2OnProperty)
                ]
            ]
        ]
    ];
    // ### END Complex struct property using raw functions ###

    // ### START Button example ###
    CategoryBuilder.AddCustomRow(LOCTEXT("Keyword", "Bool9Patch"))
    .NameContent()
    [
        SNew(STextBlock)
        .Text(LOCTEXT("NameText", "Test Button"))
        .Font(IDetailLayoutBuilder::GetDetailFont())
    ]
    .ValueContent()
    [
        SNew(SButton)
        .Text(LOCTEXT("ButtonText", "Test"))
        .ToolTipText(LOCTEXT("ButtonTooltip", "Test Button"))
        .OnClicked_Raw(this, &Bool9PatchDetails::EditObjects)
    ];
    // ### END Button example ###

    // ### START Checkbox example ###
    CategoryBuilder.AddCustomRow(LOCTEXT("Keyword", "Checkbox"))
    .NameContent()
    [
        SNew(STextBlock)
        .Text(LOCTEXT("CheckboxText", "Checkbox Test"))
        .Font(IDetailLayoutBuilder::GetDetailFont())
    ]
    .ValueContent()
    [
        SNew(SHorizontalBox)
        +
        SHorizontalBox::Slot()
        [
            SNew(SCheckBox)
            .OnCheckStateChanged_Raw(this, &Bool9PatchDetails::CheckboxChanged)
        ]
        +
        SHorizontalBox::Slot()
        [
            SNew(SCheckBox)
            .OnCheckStateChanged_Raw(this, &Bool9PatchDetails::CheckboxChanged)
        ]
        +
        SHorizontalBox::Slot()
        [
            SNew(SCheckBox)
            .OnCheckStateChanged_Raw(this, &Bool9PatchDetails::CheckboxChanged)
        ]
    ];
    // ### END Checkbox example ###
}

// Used in the Button example
FReply Bool9PatchDetails::EditObjects()
{
    UE_LOG(LogTemp, Warning, TEXT("### Working ###"));
    return FReply::Handled();
}

// Used in the Checkbox example
void Bool9PatchDetails::CheckboxChanged(ECheckBoxState CheckBoxState)
{
    UE_LOG(LogTemp, Warning, TEXT("### Checkbox changed ###"));
}

// ### START Complex struct property getters and setters
FText Bool9PatchDetails::GetVal1FromProperty() const
{
    int OutVal;
    IntPairPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FIntPairStruct, Val1))->GetValue(OutVal);
    return FText::FromString(FString::SanitizeFloat(OutVal));
}

FText Bool9PatchDetails::GetVal2FromProperty() const
{
    int OutVal;
    IntPairPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FIntPairStruct, Val2))->GetValue(OutVal);
    return FText::FromString(FString::SanitizeFloat(OutVal));
}

void Bool9PatchDetails::SetVal1OnProperty(const FText& Text, ETextCommit::Type TextCommitType) const
{
    const float Value = FCString::Atof(*Text.ToString());
    PluginExportComponentObject->IntPairStruct.Val1 = Value;
}

void Bool9PatchDetails::SetVal2OnProperty(const FText& Text, ETextCommit::Type TextCommitType) const
{
    const float Value = FCString::Atof(*Text.ToString());
    PluginExportComponentObject->IntPairStruct.Val2 = Value;
}
// ### START Complex struct property getters and setters

#undef LOCTEXT_NAMESPACE
