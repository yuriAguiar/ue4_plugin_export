// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DetailLayoutBuilder.h"
#include "PropertyEditor/Public/IDetailCustomization.h"

/**
 * 
 */
class TESTPLUGIN_API PaperGroupedSpriteInstacesHider : public IDetailCustomization
{
public:
	PaperGroupedSpriteInstacesHider();
	~PaperGroupedSpriteInstacesHider();

	static TSharedRef<IDetailCustomization> MakeInstance();
	virtual void CustomizeDetails(IDetailLayoutBuilder& DetailLayoutBuilder) override;
};
