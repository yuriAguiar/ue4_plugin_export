// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UObject/ObjectMacros.h"

#include "FIntPair.generated.h"

USTRUCT(BlueprintType)
struct FIntPairStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite) int Val1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int Val2;
};

UCLASS()
class TESTPLUGIN_API AFIntPair : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFIntPair();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
