// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DetailLayoutBuilder.h"
#include "PluginExportComponent.h"
#include "Binding/CheckedStateBinding.h"
#include "PropertyEditor/Public/IDetailCustomization.h"

class FReply;

/**
 * 
 */
class TESTPLUGIN_API Bool9PatchDetails : public IDetailCustomization
{
public:
	Bool9PatchDetails();
	~Bool9PatchDetails();

	static TSharedRef<IDetailCustomization> MakeInstance();
	virtual void CustomizeDetails(IDetailLayoutBuilder& DetailLayoutBuilder) override;
	void SetVal1OnProperty(const FText& Text, ETextCommit::Type TextCommitType) const;
	void SetVal2OnProperty(const FText& Text, ETextCommit::Type TextCommitType) const;
	void CheckboxChanged(ECheckBoxState CheckBoxState);
	FText GetVal1FromProperty() const;
	FText GetVal2FromProperty() const;
	FReply EditObjects();

private:
	TArray<TWeakObjectPtr<UObject>> Objects;
	TWeakObjectPtr<UPluginExportComponent> PluginExportComponentObject;
	TSharedPtr<class IPropertyHandle> IntPairPropertyHandle;
	TSharedPtr<class IPropertyHandle> DisplayTimePropertyHandle;
};
