// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FIntPair.h"
#include "Components/ActorComponent.h"
#include "PluginExportComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTPLUGIN_API UPluginExportComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPluginExportComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable) FString GetText() const;

	UPROPERTY(EditAnywhere)	FString Text;
	UPROPERTY(EditAnywhere) float DisplayTime;
	UPROPERTY(EditAnywhere) FIntPairStruct IntPairStruct;
};
