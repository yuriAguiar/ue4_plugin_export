// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DetailLayoutBuilder.h"
#include "PropertyEditor/Public/IDetailCustomization.h"

/**
 * 
 */
class SCISSOR2D_API InstancesCutter : public IDetailCustomization
{
public:
	InstancesCutter();
	~InstancesCutter();

	static TSharedRef<IDetailCustomization> MakeInstance();
	virtual void CustomizeDetails(IDetailLayoutBuilder& DetailLayoutBuilder) override;
};
