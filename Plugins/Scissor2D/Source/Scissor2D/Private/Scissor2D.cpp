// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Scissor2D.h"
#include "PropertyEditor/Public/PropertyEditorModule.h"
#include "PaperGroupedSpriteComponent.h"
#include "InstancesCutter.h"

#define LOCTEXT_NAMESPACE "FScissor2DModule"

void FScissor2DModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	FPropertyEditorModule& PropertyEditorModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyEditorModule.RegisterCustomClassLayout(UPaperGroupedSpriteComponent::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&InstancesCutter::MakeInstance));
}

void FScissor2DModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	FPropertyEditorModule& PropertyEditorModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyEditorModule.UnregisterCustomClassLayout(UPaperGroupedSpriteComponent::StaticClass()->GetFName());
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FScissor2DModule, Scissor2D)