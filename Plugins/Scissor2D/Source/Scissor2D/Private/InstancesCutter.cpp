// Fill out your copyright notice in the Description page of Project Settings.


#include "Scissor2D/Public/InstancesCutter.h"
#include "PropertyEditor/Public/DetailCategoryBuilder.h"
#include "PropertyEditor/Public/DetailLayoutBuilder.h"

#define LOCTEXT_NAMESPACE "PaperGroupedSpriteInstacesHider"

InstancesCutter::InstancesCutter() { }

InstancesCutter::~InstancesCutter() { }

TSharedRef<IDetailCustomization> InstancesCutter::MakeInstance()
{
    return MakeShareable(new InstancesCutter);
}

void InstancesCutter::CustomizeDetails(IDetailLayoutBuilder& DetailLayoutBuilder)
{
    IDetailCategoryBuilder& InstancesCategory = DetailLayoutBuilder.EditCategory("Instances", LOCTEXT("Instances", "Instances"), ECategoryPriority::Important);
    InstancesCategory.SetCategoryVisibility(false);
}

#undef LOCTEXT_NAMESPACE
