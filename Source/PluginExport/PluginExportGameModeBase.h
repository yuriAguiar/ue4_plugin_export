// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PluginExportGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PLUGINEXPORT_API APluginExportGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
